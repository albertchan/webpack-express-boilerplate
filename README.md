# webpack-express-boilerplate

A boilerplate for running a Webpack workflow with Node.js and Express.js.

## Features

* Express server with hot-reload in development
* Webpack 2
* Bootstrap with [bootstrap-sass](https://github.com/twbs/bootstrap-sass)
* [PostCSS Autoprefixer](https://github.com/postcss/autoprefixer)

## Installation and Usage
