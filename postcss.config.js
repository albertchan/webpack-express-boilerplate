// see https://github.com/postcss/postcss-loader

module.exports = {
  plugins: [
    require('autoprefixer')({ /* ...options */ }),
  ],
};
